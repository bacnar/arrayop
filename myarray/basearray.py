from typing import Tuple
from typing import List
from typing import Union
import array
import math

class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """
    def __init__(self, shape: Tuple[int], dtype: type=None, data: Union[Tuple, List]=None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        if not isinstance(shape, (tuple, list)):
            raise Exception("shape {shape:} is not a tuple or list")
        for v in shape:
            if not isinstance(v, int):
                raise Exception("shape {shape:} contains a non integer {v:}")

        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n = 1
        for s in self.__shape:
            n *= s

        if data is None:
            if dtype == int:
                self.__data = [0]*n
            elif dtype == float:
                self.__data = [0.0]*n
        else:
            if len(data) != n:
               raise Exception('shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception('provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    def __print__(self):
        """
        :print matrix
        """
        x = self.__shape[0]
        y = self.__shape[1]
        for i in range(0,x):
            t = ""
            for j in range(0,y):
                temp = ""
                temp += repr(self.__data[j+i*y])
                t += temp.ljust(10)
            print(t)
            t = ""

    def __sort__(self, option):
        """
        :sort matrix
        :0-po stolpcah
        :1-po vrsticah
        """
        copyA = self.__data.copy()
        result = self.__data.copy()
        #arrLenght = len(self.__data)
        if(option == 0):
            for a in range(0, self.__shape[1]):
                temp = list()
                for b in range(a, self.__shape[0]*self.__shape[1]+a-self.__shape[1]+1, self.__shape[1]):
                    temp.append(copyA[b])
                tempp = temp.copy()
                self.TopDownSplitMerge(temp, 0, self.__shape[0], tempp)
                c=0
                tempp.copy()
                for b in range(a, self.__shape[0]*self.__shape[1]+a-self.__shape[1]+1, self.__shape[1]):
                    result[b] = tempp[c]
                    c+=1
            return BaseArray((self.__shape[0], self.__shape[1]), dtype=self.__dtype, data=result)
        elif(option == 1):
            for a in range(0, self.__shape[0]):
                start = a*self.__shape[1]
                end = start + self.__shape[1]
                self.TopDownSplitMerge(copyA, start, end, result)
            return BaseArray((self.__shape[0], self.__shape[1]), dtype=self.__dtype, data=result)
        else:
            raise Exception('Wrong option')

    def TopDownSplitMerge(self, B, iBegin, iEnd, A):
        if(iEnd - iBegin < 2):                      # if run size == 1
            return                                  #   consider it sorted
        # split the run longer than 1 item into halves
        iMiddle = int((iEnd + iBegin) / 2)               # iMiddle = mid point
        # recursively sort both runs from array A[] into B[]
        self.TopDownSplitMerge(A, iBegin,  iMiddle, B)   # sort the left  run
        self.TopDownSplitMerge(A, iMiddle,    iEnd, B)   # sort the right run
        # merge the resulting runs from array B[] into A[]
        self.TopDownMerge(B, iBegin, iMiddle, iEnd, A)

    def TopDownMerge(self, A, iBegin, iMiddle, iEnd, B):
        i = int(iBegin)
        j = int(iMiddle)
        # While there are elements in the left or right runs...
        for k in range(int(iBegin), int(iEnd)):
            # If left run head exists and is <= existing right run head.
            if(int(i) < int(iMiddle) and (int(j) >= int(iEnd) or A[i] <= A[j])):
                B[k] = A[i]
                i = i + 1
            else:
                B[k] = A[j]
                j = j + 1

    def __search__(self, value):
        y = self.__shape[1]
        i = len(self.__data)
        col = 0
        row = 0
        temp = "("
        for j in range(0, i):
            if (self.__data[j] == value):
                row = int(j/y)
                col = int(j%y)
                temp += "("
                temp += str(row)
                temp += ","
                temp += str(col)
                temp += "), "
        temp += ")"
        return temp

    def __sum__(self, ses):
        """
        :Sestevanje skalarja ali matrike z matriko
        """
        temp = self
        if(isinstance(ses, BaseArray)):     #je matrika
            if(ses.__shape == self.__shape):
                i=0
                for a in ses:
                    temp.__data[i] += a
                    i += 1
            else:
                raise Exception("Velikosti se ne ujemata")
        else:                               #je skalar
            for a in range(0, len(temp.__data)):
                temp.__data[a] += ses
        return temp

    def __desum__(self, ses):
        """
        :Odstevanje skalarja ali matrike z matriko
        """
        temp = self
        if(isinstance(ses, BaseArray)):     #je matrika
            if(ses.__shape == self.__shape):
                i=0
                for a in ses:
                    temp.__data[i] -= a
                    i += 1
            else:
                raise Exception("Velikosti se ne ujemata")
        else:                               #je skalar
            for a in range(0, len(temp.__data)):
                temp.__data[a] -= ses
        return temp

    def __multi__(self, ses):
        """
        :Mnozenje skalarja ali matrike z matriko
        """
        temp = self
        if(isinstance(ses, BaseArray)):     #je matrika
            temp = BaseArray((ses.__shape[1], self.__shape[0]), dtype=self.__dtype)
            if(ses.__shape[1] == self.__shape[0]):
                # iterate through rows of X
                for i in range(0, self.__shape[0]):
                    # iterate through columns of Y
                    for j in range(0, ses.__shape[1]):
                        # iterate through rows of Y
                        for k in range(ses.__shape[0]):
                            temp.__data[(i*ses.__shape[1]) + j%ses.__shape[1]] += self.__data[i*self.__shape[1] + k] * ses.__data[k * ses.__shape[1] + j]
            else:
                raise Exception("Velikosti se ne ujemata")
        else:                               #je skalar
            for a in range(0, len(temp.__data)):
                temp.__data[a] *= ses
        return temp

    def __div__(self, ses):
        """
        :Deljenje skalarja ali matrike z matriko
        """
        temp = self
        if(isinstance(ses, BaseArray)):     #je matrika
            temp = BaseArray((ses.__shape[1], self.__shape[0]), dtype=self.__dtype)
            if(ses.__shape == self.__shape):
                i=0
                for a in ses:
                    temp.__data[i] /= a
                    i += 1
            else:
                raise Exception("Velikosti se ne ujemata")
        else:                               #je skalar
            for a in range(0, len(temp.__data)):
                temp.__data[a] /= ses
        return temp

    def __exp__(self, ses):
        """
        :potenciranje matrike
        """
        temp = self
        for a in range(0, len(temp.__data)):
            temp.__data[a] = math.pow(temp.__data[a], ses)
        return temp

    def __log__(self):
        """
        :logaritmiranje matrike
        """
        temp = self
        for a in range(0, len(temp.__data)):
            temp.__data[a] = math.log(temp.__data[a])
        return temp

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 0 or ind[ax] >= self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds (0, {:})'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not type(indice) is tuple:
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception('{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception('indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not type(indice) is tuple:
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception('{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception('indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data


def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if type(inds) is slice:
        inds_itt = range(s)[inds]
    elif type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,)+ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += inds[n]*s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if len(indice) == 0:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True
