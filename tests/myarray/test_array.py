from unittest import TestCase
#from myarrayMain.basearray import BaseArray
from myarrayMain.basearray import BaseArray
import myarrayMain.basearray as ndarray


class TestArray(TestCase):
    def test___init__(self):

        # simple init tests, other things are tester later
        try:
            a = BaseArray((2,))
            a = BaseArray([2, 5])
            a = BaseArray((2, 5), dtype=int)
            a = BaseArray((2, 5), dtype=float)
        except Exception as e:
            self.fail("basic constructor test failed, with exception {e:}")

        # test invalid parameters as shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray(5)
        self.assertEqual(('shape 5 is not a tuple or list',),
                         cm.exception.args)

        # test invalid value in shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray((4, 'a'))
        self.assertEqual(('shape (4, \'a\') contains a non integer a',),
                         cm.exception.args)

        # TODO test invalid dtype

        # test valid data param, list, tuple
        a = BaseArray((3, 4), dtype=int, data=list(range(12)))
        a = BaseArray((3, 4), dtype=int, data=tuple(range(12)))

        # test invalid data param
        # invalid data length
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 4), dtype=int, data=list(range(12)))
        self.assertEqual(('shape (2, 4) does not match provided data length 12',),
                         cm.exception.args)

        # inconsistent data type
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 2), dtype=int, data=(4, 'a', (), 2.0))
        self.assertEqual(('provided data does not have a consistent type',),
                         cm.exception.args)

    def test_print(self):
        try:
            a = BaseArray((2, 3), dtype=float, data=(1., 2., 1., 2., 1., 2.))
            a.__print__()
        except Exception as e:
            self.fail("Print not working")

    def test_sort_col(self):
        a = BaseArray((3, 3), dtype=float, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        a_vals = [v for v in a.__sort__(0)]
        self.assertListEqual(a_vals, [0, 0, -2, 1, 1, 0, 4, 3, 1])

    def test_sort_row(self):
        a = BaseArray((3, 3), dtype=float, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        a_vals = [v for v in a.__sort__(1)]
        self.assertListEqual(a_vals, [0, 1, 1, -2, 0, 4, 0, 1, 3])

    def test_search(self):
        a = BaseArray((3, 3), dtype=float, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        self.assertEqual(a.__search__(2), "((1,0), (2,1), (2,2), )")

    def test_sum_skalar(self):
        a = BaseArray((3, 3), dtype=float, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        a_vals = [v for v in a.__sum__(2)]
        self.assertListEqual(a_vals, [2, 2, 2, 4, 2, 2, 2, 4, 4])

    def test_sum_matrics(self):
        a = BaseArray((3, 3), dtype=float, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        b = BaseArray((3, 3), dtype=float, data=(3, 0, 0, 4, 0, 0, 6, 0, 0))
        a_vals = [v for v in a.__sum__(b)]
        self.assertListEqual(a_vals, [3, 0, 0, 6, 0, 0, 6, 2, 2])

    def test_desum_skalar(self):
        a = BaseArray((3, 3), dtype=float, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        a_vals = [v for v in a.__desum__(2)]
        self.assertListEqual(a_vals, [-2, -2, -2, 0, -2, -2, -2, 0, 0])

    def test_desum_matrics(self):
        a = BaseArray((3, 3), dtype=float, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        b = BaseArray((3, 3), dtype=float, data=(3, 0, 0, 4, 0, 0, 6, 0, 0))
        a_vals = [v for v in a.__desum__(b)]
        self.assertListEqual(a_vals, [-3, 0, 0, -2, 0, 0, -6, 2, 2])

    def test_mul_skalar(self):
        a = BaseArray((3, 3), dtype=float, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        a_vals = [v for v in a.__multi__(2)]
        self.assertListEqual(a_vals, [0, 0, 0, 4, 0, 0, 0, 4, 4])

    def test_mul_matrics(self):
        a = BaseArray((2, 3), dtype=float, data=(1, 2, 1, 2, 1, 2))
        b = BaseArray((3, 2), dtype=float, data=(1, 2, 1, 2, 1, 2))
        a_vals = [v for v in a.__multi__(b)]
        self.assertListEqual(a_vals, [4, 8, 5, 10])

    def test_div_skalar(self):
        a = BaseArray((3, 3), dtype=float, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        a_vals = [v for v in a.__div__(2)]
        self.assertListEqual(a_vals, [0, 0, 0, 1, 0, 0, 0, 1, 1])

    def test_div_matrics(self):
        a = BaseArray((2, 3), dtype=float, data=(1, 2, 1, 2, 1, 2))
        b = BaseArray((2, 3), dtype=float, data=(1, 2, 1, 2, 1, 2))
        a_vals = [v for v in a.__div__(b)]
        self.assertListEqual(a_vals, [0, 0, 0, 0, 0, 0])

    def test_exp(self):
        a = BaseArray((3, 3), dtype=float, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        a_vals = [v for v in a.__exp__(2)]
        self.assertListEqual(a_vals, [0, 0, 0, 4, 0, 0, 0, 4, 4])

    def test_log(self):
        a = BaseArray((2, 2), dtype=float, data=(2,1,3,4))
        a_vals = [v for v in a.__log__()]
        self.assertListEqual(a_vals, [0.6931471805599453, 0.0, 1.0986122886681098, 1.3862943611198906])

    def test_shape(self):
        a = BaseArray((5,))
        self.assertTupleEqual(a.shape, (5,))
        a = BaseArray((2, 5))
        self.assertTupleEqual(a.shape, (2, 5))
        a = BaseArray((3, 2, 5))
        self.assertEqual(a.shape, (3, 2, 5))

    def test_dtype(self):
        # test if setting a type returns the set type
        a = BaseArray((1,))
        self.assertIsInstance(a[0], float)
        a = BaseArray((1,), int)
        self.assertIsInstance(a[0], int)
        a = BaseArray((1,), float)
        self.assertIsInstance(a[0], float)

    def test_get_set_item(self):
        # test if setting a value returns the value
        a = BaseArray((1,))
        a[0] = 1
        self.assertEqual(1, a[0])
        # test for multiple dimensions
        a = BaseArray((2, 1))
        a[0, 0] = 1
        a[1, 0] = 1
        self.assertEqual(1, a[0, 0])
        self.assertEqual(1, a[1, 0])

        # test with invalid indices
        a = BaseArray((2, 2))
        with self.assertRaises(Exception) as cm:
            a[3, 0] = 0
        self.assertEqual(('indice (3, 0), axis 0 out of bounds (0, 2)',),
                         cm.exception.args)

        # test invalid type of ind
        with self.assertRaises(Exception) as cm:
            a[1, 1.1] = 0
        self.assertEqual(cm.exception.args,
                         ('(1, 1.1) is not a valid indice',))

        with self.assertRaises(Exception) as cm:
            a[1, 'a'] = 0
        self.assertEqual(('(1, \'a\') is not a valid indice',),
                         cm.exception.args)

        # test invalid number of ind
        with self.assertRaises(Exception) as cm:
            a[1] = 0
        self.assertEqual(('indice (1,) is not valid for this myarray',),
                         cm.exception.args)

        # test data intialized via data parameter
        a = BaseArray((2, 3, 4), dtype=int, data=tuple(range(24)))
        self.assertEqual(3, a[0, 0, 3])
        self.assertEqual(13, a[1, 0, 1])

        # TODO enter invalid type


    def test_iter(self):
        a = BaseArray((2, 3), dtype=int)
        a[0, 0] = 1
        a[0, 1] = 2
        a[0, 2] = 3
        a[1, 0] = 4
        a[1, 1] = 5
        a[1, 2] = 6

        a_vals = [v for v in a]
        self.assertListEqual([1, 2, 3, 4, 5, 6], a_vals)

    def test_reversed(self):
        a = BaseArray((2, 3), dtype=int)
        a[0, 0] = 1
        a[0, 1] = 2
        a[0, 2] = 3
        a[1, 0] = 4
        a[1, 1] = 5
        a[1, 2] = 6

        a_vals = [v for v in reversed(a)]
        self.assertListEqual([6, 5, 4, 3, 2, 1], a_vals)

    def test_contains(self):
        a = BaseArray((1,), dtype=float)
        a[0] = 1

        self.assertIn(1., a)
        self.assertIn(1, a)
        self.assertNotIn(0, a)

    def test_shape_to_steps(self):
        shape = (4, 2, 3)
        steps_exp = (6, 3, 1)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = [1]
        steps_exp = (1,)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = []
        steps_exp = ()
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

    def test_multi_to_lin_ind(self):
        # shape = 4, 2,
        # 0 1
        # 2 3
        # 4 5
        # 6 7
        steps = 2, 1
        multi_lin_inds_pairs = (((0, 0), 0),
                                ((1, 1), 3),
                                ((3, 0), 6),
                                ((3, 1), 7))
        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_exp, lin_ind_res)

        # shape = 2, 3, 4
        #
        # 0 1 2 3
        # 4 5 6 7
        #
        #  8  9 10 11
        # 12 13 14 15
        steps = 8, 4, 1
        multi_lin_inds_pairs = (((0, 0, 0), 0),
                                ((1, 1, 0), 12),
                                ((0, 0, 3), 3),
                                ((1, 1, 2), 14))

        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_res, lin_ind_exp)

    def test_is_valid_instance(self):
        self.assertTrue(ndarray._is_valid_indice(1))
        self.assertTrue(ndarray._is_valid_indice((1,0)))

        self.assertFalse(ndarray._is_valid_indice(()))
        self.assertFalse(ndarray._is_valid_indice([1]))
        self.assertFalse(ndarray._is_valid_indice(1.0))
        self.assertFalse(ndarray._is_valid_indice((1, 2.)))

    def test_multi_ind_iterator(self):
        shape = (30,)
        ind = (slice(10, 20, 3),)
        ind_list_exp = ((10,),
                        (13,),
                        (16,),
                        (19,))
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        shape = (5, 5, 5)
        ind = (2, 2, 2)
        ind_list_exp = ((2, 2, 2),)
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (2, slice(3), 2)
        ind_list_exp = ((2, 0, 2),
                        (2, 1, 2),
                        (2, 2, 2),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (slice(None, None, 2), slice(3), slice(2, 4))
        ind_list_exp = ((0, 0, 2), (0, 0, 3),
                        (0, 1, 2), (0, 1, 3),
                        (0, 2, 2), (0, 2, 3),
                        (2, 0, 2), (2, 0, 3),
                        (2, 1, 2), (2, 1, 3),
                        (2, 2, 2), (2, 2, 3),
                        (4, 0, 2), (4, 0, 3),
                        (4, 1, 2), (4, 1, 3),
                        (4, 2, 2), (4, 2, 3),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)



