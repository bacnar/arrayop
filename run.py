from myarrayMain.basearray import BaseArray
import cProfile, pstats
import io
import random

arr=[]
for i in range (1000000):
    arr.append(random.randrange(1, 101, 1))

a = BaseArray((1000, 1000), dtype=float, data=arr)
#a.__print__()
#po vrsticah
#c = a.__sort__(1)
#c.__print__()

pr = cProfile.Profile()
pr.enable()
c = a.__sort__(1)
pr.disable()
s = io.StringIO()
sortby = 'cumulative'
ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
ps.print_stats()
f = open("otpu.txt", "w")
f.write(s.getvalue())